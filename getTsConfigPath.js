const path = require('path');
const fs = require('fs');

function getTsConfigPath() {
  const tsConfigPath = path.join(process.cwd(), 'tsconfig.json');

  if (!fs.existsSync(tsConfigPath)) {
    throw Error(`There is no tsconfig in path: ${tsConfigPath}`);
  }

  return tsConfigPath;
}

module.exports = getTsConfigPath;
