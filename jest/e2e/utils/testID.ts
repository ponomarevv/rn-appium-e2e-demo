import { Platform } from 'react-native';

export default function testID(id: string) {

  const testIdProps = {
    testID: id
  };

  if (__DEV__ && Platform.OS === 'android') {
    return {
      ...testIdProps,
      accessible: true,
      accessibilityLabel: id
    }
  }

  return testIdProps;
}
