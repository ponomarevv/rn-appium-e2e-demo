import get from 'lodash/get';
import has from 'lodash/has';
import path from 'path';
import pkg from'../../package.json';

const { devices } = pkg.e2e;

if (!process.env.E2E_DEVICE) {
  throw new Error('E2E_DEVICE environment variable is not defined');
}

if (!has(devices, process.env.E2E_DEVICE)) {
  throw new Error(`No e2e device configuration found in package.json for E2E_DEVICE environment ${process.env.E2E_DEVICE}`);
}

let device = get(devices, process.env.E2E_DEVICE);

if (device.hasOwnProperty('app')) {
  if (path.resolve( device.app ) !== path.normalize( device.app )) {
    device = {
      ...device,
      app: path.resolve(process.cwd(), path.normalize(device.app))
    }
  }
}

export default device;
