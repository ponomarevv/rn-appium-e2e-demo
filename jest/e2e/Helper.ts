import config from './e2e-config';
import * as wd from 'wd';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

type webDriverOptions = {
  port: number
}

export default class Helper {
  public driver: any = null;

  constructor(options?: webDriverOptions) {
    const port = options && options.port ? options.port : 4723;

    this.platform = config.platformName.toString().toLowerCase();
    this.driver = wd.promiseChainRemote('localhost', port);

    if (this.platform = Helper.platformNames.android) {
      this.driver.elementById = this.driver.elementByAccessibilityId;
      this.driver.elementByIdOrNull = this.driver.elementByAccessibilityIdOrNull;
      this.driver.elementByIdIfExists = this.driver.elementByAccessibilityIdIfExists;
      this.driver.hasElementById = this.driver.hasElementByAccessibilityId;
      this.driver.waitForElementById = this.driver.waitForElementByAccessibilityId;
      this.driver.waitForElementsById = this.driver.waitForElementsByAccessibilityId;
      this.driver.elementsById = this.driver.elementsByAccessibilityId;
    }
  }

  public async setup() {
    return this.driver.init(config);
  }

  public async teardown() {
    return this.driver.quit()
  }

  private platform: string;
  private static platformNames = {
    android: 'android',
    ios: 'ios',
  }
}
