const babelJest = require('babel-jest');
const tsTransformer = require('react-native-typescript-transformer');
// tslint:disable-next-line:max-line-length
// @see https://github.com/ds300/react-native-typescript-transformer/issues/21#issuecomment-330148700
const tsPreprocessor = {
  process(src, file, config, transformOptions) {
    return tsTransformer.transform({
      filename: file,
      localPath: file,
      options: {
        dev: true,
        inlineRequires: true,
        platform: '',
        projectRoot: '',
        retainLines: true,
      },
      src,
    }).code;
  },
};

const preprocessor = Object.assign({}, {
  getCacheKey: babelJest.getCacheKey,
  process(src, file, config, transformOptions) {
    src = tsPreprocessor.process(src, file, config, transformOptions);
    src = babelJest.process(src, file, config, transformOptions);

    return src;
  },
});

module.exports = preprocessor;
