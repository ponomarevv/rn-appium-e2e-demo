const path = require('path');
const TsConfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const getTsConfigPath = require('./getTsConfigPath');

const tsConfigPaths = new TsConfigPathsPlugin({
  configFileName: getTsConfigPath(),
});

const tsConfigAlias = tsConfigPaths.mappings.reduce((all, item) => {
  const { alias, target } = item;
  const aliasPath = path.join(process.cwd(), target);

  return Object.assign({}, all, { [alias]: aliasPath });
}, {});

module.exports = {
  resolve: {
    alias: tsConfigAlias,
  },
};
