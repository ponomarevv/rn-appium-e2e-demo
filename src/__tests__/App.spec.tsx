import * as React from 'react';
import 'react-native';
import App from '../App';

// Note: test renderer must be required after react-native.
import { shallow } from 'enzyme';

describe('App', () => {

  it('renders correctly', () => {
    const tree = shallow(
      <App />
    );
    expect(tree).toMatchSnapshot();
  });

});
