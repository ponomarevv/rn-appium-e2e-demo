import { Helper } from '../jest/e2e';
const helper = new Helper();

describe('Simple Appium Example', () => {
  beforeAll(async () => await helper.setup());
  afterAll(async () => await helper.teardown());

  it('has welcome text', async () => {
    await helper.driver.waitForElementById('welcome_text');
    expect(await helper.driver.hasElementById('welcome_text')).toBe(true);
  });
});
