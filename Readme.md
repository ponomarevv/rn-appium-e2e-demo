# React Native Appium E2E demo

Proof-of-concept E2E тестов на jest + typescript и appium

В качестве движка для организации e2e-тестирования выбран именно appium т.к. на данный момент он де-факто является стандартом для blackbox/whitebox тестирования мобильных приложений.
Основными особенностями данного инструмента являются

- Основан на Selenium / WebDriver (стандарт W3C)
- Поддерживает основные нативные движки для тестирования (в т.ч. [Espresso][*] на Android и UIAutomation на iOS)
- При необходимости масштабируется ([AWS][1] / Browserstack / [Bitbar][2] и прочие)

[*]: https://github.com/appium/appium-espresso-driver "WIP early stage"
[1]: https://aws.amazon.com/ru/blogs/mobile/test-ios-apps-on-aws-device-farm-using-appium-part-1-prerequisities-environment-set-up-and-test-creation/ "только для тестов на JUnit/TestNG/Python/Calaba.sh"
[2]: https://bitbar.com/37-things-you-should-know-about-appium/ "язык любой из тех что поддерживает appium"

## Installation
``` 
git clone <repository>/rn-appium-e2e-demo
cd rn-appium-e2e-demo

gem install bundler
bundle
rake
```

## Examples
1. Запустить консольную версию appium или установить и запустить [appium-desktop](https://github.com/appium/appium-desktop/releases/latest)
```
rake appium:start
```

2. Запустить ios-simulator и android-emulator

3. Запустить примеры
```
rake samples
```
или
```
rake samples:e2e_ios
rake samples:e2e_android
```

## Known issues
На данный момент известные способы e2e тестирования RN-приложений на android сводятся к использованию accessibilityLabel для обращения из тестовой среды. TestID в Android не передается в натив.
Это неправильно, т.к. accessibilityLabel виден пользователям при использовании [специальных возможностей для людей с ограничениями зрения и т.п.](https://habrahabr.ru/company/touchinstinct/blog/328894/)

В [issue на Github](https://github.com/facebook/react-native/pull/9942) подробно расписано, в чем причина данного решения и периодически сообщество пытается продавить разработчиков на решение данной проблемы

В связи с этим сообществом был предложен [обходной](https://github.com/facebook/react-native/pull/9942#issuecomment-266992184) [вариант](https://github.com/isnifer/react-native-testid) который предполагает в том числе дополнительный шаг для Android - сборку приложения для e2e тестирования.

Это позволяет избежать публикации тестовых переменных (они присутствуют только в debug / e2e сборках).

## Read
[Документация Appium](https://github.com/appium/appium/tree/master/docs)

[Серия статей по appium от Bitbar](https://bitbar.com/37-things-you-should-know-about-appium/)

[Документация api WD](https://github.com/admc/wd/blob/master/doc/api.md)

[React Native e2e Test Example](https://github.com/garthenweb/react-native-e2etest)
