def shell_exec(commands)
  commands.each do |cmd|
    `#{cmd}`
  end
end

namespace :appium do

  desc "Initialize appium main dependencies"
  task :init do
    cmds = [
      'yarn global add appium@beta', # android issue `unlock_apk-debug.apk [INSTALL_FAILED_VERSION_DOWNGRADE]`
                                     # see https://github.com/appium/appium/issues/9497
      'yarn global add appium-doctor',
    ]
    shell_exec cmds
  end

  desc "Initialize appium android dependencies"
  task :android do
    cmds = [
      'appium-doctor --android',
    ]
    shell_exec cmds
  end

  desc "Initialize appium ios dependencies"
  task :ios do
    cmds = [
      'brew install carthage',
      'appium-doctor --ios',
    ]
    shell_exec cmds
  end

  desc "Initialize appium ios dependencies and devices libraries"
  task :ios_devices => :ios do
    cmds = [
      'brew install libimobiledevice',
      'yarn global add ios-deploy',
    ]
    shell_exec cmds
  end

  desc "Run appium server"
  task :start do
    cmds = [
      'yarn appium:start',
    ]
    shell_exec cmds
  end

  task :all => [:init, :android, :ios_devices]
end

namespace :bundle do

  desc "Initialize project node & ruby bundle"
  task :install do
    cmds = [
      'yarn install',
    ]
    shell_exec cmds
  end

end

namespace :samples do

  task :build_ios do
    cmds = [
      'yarn build:ios:e2e'
    ]
    shell_exec cmds
  end

  desc "Build iOS app and run e2e spec"
  task :e2e_ios => :build_ios do
    cmds = [
      'yarn test:e2e-ios'
    ]
    shell_exec cmds
  end

  task :build_android do
    cmds = [
      'yarn build:android:e2e'
    ]
    shell_exec cmds
  end

  desc "Build Android app and run e2e spec"
  task :e2e_android => :build_android do
    cmds = [
      'yarn test:e2e-android'
    ]
    shell_exec cmds
  end

  task :all => [:e2e_ios, :e2e_android]
end

desc "Initialize appium with all dependencies"
task :appium => 'appium:all'

desc "Initialize project"
task :bundle => 'bundle:install'

desc "Run samples"
task :samples => 'samples:all'

task :default => [:bundle, :appium]
